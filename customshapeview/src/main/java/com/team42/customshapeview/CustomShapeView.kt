package com.team42.customshapeview

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.FrameLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.ViewCompat
import com.team42.customshapeview.Managers.ClipDrawableManager
import com.team42.customshapeview.Managers.ClipPathManager
import com.team42.customshapeview.Managers.interfaces.ClipManager
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator


open class CustomShapeView : FrameLayout {

    private var clipManager: ClipManager? = null

    private val clipPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    protected var pdMode: PorterDuffXfermode? = null

    protected var mask: Bitmap? = null

    constructor(context: Context) : super(context) {
        init(context, null);
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet, 0) {
        init(context, attributeSet);
    }

    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int) : super(
        context,
        attributeSet,
        defStyle
    ) {
        init(context, attributeSet)
    }

    private fun init(context: Context, attr: AttributeSet?) {

        clipPaint.setAntiAlias(true)
        clipPaint.setColor(Color.WHITE)

        setDrawingCacheEnabled(true)
        setLayerType(LAYER_TYPE_SOFTWARE, null) //Only works for software layers

        pdMode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)
        setWillNotDraw(false)

        clipPaint.setColor(Color.BLACK)
        clipPaint.setStyle(Paint.Style.FILL_AND_STROKE)
        clipPaint.setStrokeWidth(1f)
        if (attr != null) {
            val attributes: TypedArray =
                context.obtainStyledAttributes(attr, R.styleable.CustomShapeView)
            if (attributes.hasValue(R.styleable.CustomShapeView_clip_drawable)) {
                val resourceId: Int =
                    attributes.getResourceId(R.styleable.CustomShapeView_clip_drawable, -1)
                if (-1 != resourceId) {
                    setDrawable(resourceId)
                }
            }
            attributes.recycle()
        }
    }


    protected fun dpToPx(dp: Float): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp,
            Resources.getSystem().getDisplayMetrics()
        ).toInt()
    }

    override fun onLayout(
        changed: Boolean,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        super.onLayout(changed, left, top, right, bottom)
        if (changed) {
            calculateLayout()
        }
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        clipPaint.xfermode = pdMode
        canvas.drawBitmap(mask!!, 0.0f, 0.0f, clipPaint)
        clipPaint.xfermode = null
    }

    private fun calculateLayout() {
        if (clipManager != null) {
            val height = measuredHeight
            val width = measuredWidth
            if (width > 0 && height > 0) {
                if (mask != null && !mask!!.isRecycled) {
                    mask!!.recycle()
                }
                if (clipManager != null) {
                    clipManager!!.setupClipLayout(width, height)
                    mask = clipManager!!.createMask(width, height)
                }
                //this needs to be fixed for 25.4.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && ViewCompat.getElevation(
                        this
                    ) > 0f
                ) {
                    try {
                        outlineProvider = outlineProvider
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
        postInvalidate()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getOutlineProvider(): ViewOutlineProvider? {
        return object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline) {
                if (clipManager != null) {
                    val shadowConvexPath = clipManager!!.getShadowConvexPath()
                    if (shadowConvexPath != null) {
                        try {
                            outline.setConvexPath(shadowConvexPath)
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }
    }


    fun setClipPathCreator(createClipPath: ClipPathCreator?) {
        if (clipManager !is ClipPathManager) {
            clipManager = ClipPathManager()
        }
        (clipManager as ClipPathManager).setClipPathCreator(createClipPath)
    }

    private fun setDrawable(resourceId: Int) {
        setDrawable((AppCompatResources.getDrawable(getContext(), resourceId)!!))
    }

    private fun setDrawable(drawable: Drawable) {

        if ((clipManager !is ClipDrawableManager)) {
            clipManager = ClipDrawableManager()
        }
        (clipManager as ClipDrawableManager).setDrawable(drawable)
    }
}