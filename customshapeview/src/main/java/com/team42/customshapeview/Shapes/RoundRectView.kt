package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R

class RoundRectView : CustomShapeView {

    private val rectF = RectF()

    private var topLeftDiameter = 0
    private var topRightDiameter = 0
    private var bottomRightDiameter = 0
    private var bottomLeftDiameter = 0


    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        if (attrs != null) {
            val attributes: TypedArray = context.obtainStyledAttributes(R.styleable.RoundRectView)
            topLeftDiameter = attributes.getDimensionPixelSize(
                R.styleable.RoundRectView_roundRect_topLeftDiameter,
                topLeftDiameter
            )
            topRightDiameter = attributes.getDimensionPixelSize(
                R.styleable.RoundRectView_roundRect_topRightDiameter,
                topRightDiameter
            )
            bottomLeftDiameter = attributes.getDimensionPixelSize(
                R.styleable.RoundRectView_roundRect_bottomLeftDiameter,
                bottomLeftDiameter
            )
            bottomRightDiameter = attributes.getDimensionPixelSize(
                R.styleable.RoundRectView_roundRect_bottomRightDiameter,
                bottomRightDiameter
            )
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                rectF.set(0f, 0f, width.toFloat(), height.toFloat())
                return generatePath(
                    rectF,
                    topLeftDiameter.toFloat(),
                    topRightDiameter.toFloat(),
                    bottomRightDiameter.toFloat(),
                    bottomLeftDiameter.toFloat()
                )
            }

        })
    }

    private fun generatePath(
        rect: RectF,
        topLeftDiameter: Float,
        topRightDiameter: Float,
        bottomRightDiameter: Float,
        bottomLeftDiameter: Float
    ): Path? {

        val path = Path()

        val TLD: Float = if (topLeftDiameter < 0) 0f else topLeftDiameter
        val TRD: Float = if (topRightDiameter < 0) 0f else topRightDiameter
        val BLD: Float = if (bottomLeftDiameter < 0) 0f else bottomLeftDiameter
        val BRD: Float = if (bottomRightDiameter < 0) 0f else bottomRightDiameter

        path.moveTo(rect.left + TLD, rect.top)
        path.lineTo(rect.right - TRD, rect.top)
        path.quadTo(rect.right, rect.top, rect.right, rect.top + TRD)
        path.lineTo(rect.right, rect.bottom - BRD)
        path.quadTo(rect.right, rect.bottom, rect.right - BRD, rect.bottom)
        path.lineTo(rect.left + BLD, rect.bottom)
        path.quadTo(rect.left, rect.bottom, rect.left, rect.bottom - BLD)
        path.lineTo(rect.left, rect.top + TLD)
        path.quadTo(rect.left, rect.top, rect.left + TLD, rect.top)
        path.close()

        return path
    }

    fun getTopLeftDiameter(): Int {
        return topLeftDiameter
    }

    fun setTopLeftDiameter(topLeftDiameter: Int) {
        this.topLeftDiameter = topLeftDiameter
        postInvalidate()
    }

    fun getTopRightDiameter(): Int {
        return topRightDiameter
    }

    fun setTopRightDiameter(topRightDiameter: Int) {
        this.topRightDiameter = topRightDiameter
        postInvalidate()
    }

    fun getBottomRightDiameter(): Int {
        return bottomRightDiameter
    }

    fun setBottomRightDiameter(bottomRightDiameter: Int) {
        this.bottomRightDiameter = bottomRightDiameter
        postInvalidate()
    }

    fun getBottomLeftDiameter(): Int {
        return bottomLeftDiameter
    }

    fun setBottomLeftDiameter(bottomLeftDiameter: Int) {
        this.bottomLeftDiameter = bottomLeftDiameter
        postInvalidate()
    }
}