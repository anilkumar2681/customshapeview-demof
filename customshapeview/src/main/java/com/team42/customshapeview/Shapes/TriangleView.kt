package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R


class TriangleView : CustomShapeView {

    private var percentBottom = 0.5f
    private var percentLeft = 0f
    private var percentRight = 0f

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        if (attrs != null) {
            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.TriangleView)
            percentBottom =
                attributes.getFloat(R.styleable.TriangleView_triangle_percentBottom, percentBottom)
            percentLeft =
                attributes.getFloat(R.styleable.TriangleView_triangle_percentLeft, percentLeft)
            percentRight =
                attributes.getFloat(R.styleable.TriangleView_triangle_percentRight, percentRight)
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                val path = Path()

                path.moveTo(0f, percentLeft * height)
                path.lineTo(percentBottom * width, height.toFloat())
                path.lineTo(width.toFloat(), percentRight * height)
                path.close()

                return path
            }

        })
    }

    fun getPercentBottom(): Float {
        return percentBottom
    }

    fun setPercentBottom(percentBottom: Float) {
        this.percentBottom = percentBottom
        postInvalidate()
    }

    fun getPercentLeft(): Float {
        return percentLeft
    }

    fun setPercentLeft(percentLeft: Float) {
        this.percentLeft = percentLeft
        postInvalidate()
    }

    fun getPercentRight(): Float {
        return percentRight
    }

    fun setPercentRight(percentRight: Float) {
        this.percentRight = percentRight
        postInvalidate()
    }
}