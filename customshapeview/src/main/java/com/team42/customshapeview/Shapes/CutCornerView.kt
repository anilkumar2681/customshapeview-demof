package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R


class CutCornerView : CustomShapeView {

    private val rectF = RectF()

    private var topLeftCutSize = 0
    private var topRightCutSize = 0
    private var bottomRightCutSize = 0
    private var bottomLeftCutSize = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        if (attrs != null) {
            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.CutCornerView)

            topLeftCutSize = attributes.getDimensionPixelSize(
                R.styleable.CutCornerView_cutCorner_topLeftSize,
                topLeftCutSize
            )
            topRightCutSize = attributes.getDimensionPixelSize(
                R.styleable.CutCornerView_cutCorner_topRightSize,
                topRightCutSize
            )
            bottomLeftCutSize = attributes.getDimensionPixelSize(
                R.styleable.CutCornerView_cutCorner_bottomLeftSize,
                bottomLeftCutSize
            )
            bottomRightCutSize = attributes.getDimensionPixelSize(
                R.styleable.CutCornerView_cutCorner_bottomRightSize,
                bottomRightCutSize
            )
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                rectF.set(0f, 0f, width.toFloat(), height.toFloat())
                return generatePath(
                    rectF,
                    topLeftCutSize.toFloat(),
                    topRightCutSize.toFloat(),
                    bottomRightCutSize.toFloat(),
                    bottomLeftCutSize.toFloat()
                )
            }

        })
    }

    private fun generatePath(
        rect: RectF,
        topLeftDiameter_param: Float,
        topRightDiameter_param: Float,
        bottomLeftDiameter_param: Float,
        bottomRightDiameter_param: Float
    ): Path? {
        val path = Path()

        val topLeftDiameter = if (topLeftDiameter_param < 0) 0f else topLeftDiameter_param
        val topRightDiameter = if (topRightDiameter_param < 0) 0f else topRightDiameter_param
        val bottomLeftDiameter = if (bottomLeftDiameter_param < 0) 0f else bottomLeftDiameter_param
        val bottomRightDiameter =
            if (bottomRightDiameter_param < 0) 0f else bottomRightDiameter_param

        path.moveTo(rect.left + topLeftDiameter, rect.top)
        path.lineTo(rect.right - topRightDiameter, rect.top)
        path.lineTo(rect.right, rect.top + topRightDiameter)
        path.lineTo(rect.right, rect.bottom - bottomRightDiameter)
        path.lineTo(rect.right - bottomRightDiameter, rect.bottom)
        path.lineTo(rect.left + bottomLeftDiameter, rect.bottom)
        path.lineTo(rect.left, rect.bottom - bottomLeftDiameter)
        path.lineTo(rect.left, rect.top + topLeftDiameter)
        path.lineTo(rect.left + topLeftDiameter, rect.top)
        path.close()

        return path
    }

    fun getTopLeftCutSize(): Int {
        return topLeftCutSize
    }

    fun setTopLeftCutSize(topLeftCutSize: Int) {
        this.topLeftCutSize = topLeftCutSize
        postInvalidate()
    }

    fun getTopRightCutSize(): Int {
        return topRightCutSize
    }

    fun setTopRightCutSize(topRightCutSize: Int) {
        this.topRightCutSize = topRightCutSize
        postInvalidate()
    }

    fun getBottomRightCutSize(): Int {
        return bottomRightCutSize
    }

    fun setBottomRightCutSize(bottomRightCutSize: Int) {
        this.bottomRightCutSize = bottomRightCutSize
        postInvalidate()
    }

    fun getBottomLeftCutSize(): Int {
        return bottomLeftCutSize
    }

    fun setBottomLeftCutSize(bottomLeftCutSize: Int) {
        this.bottomLeftCutSize = bottomLeftCutSize
        postInvalidate()
    }
}