package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R

class StarView : CustomShapeView {

    private var noOfPoints = 5

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val attributes: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.StarView)
            val points: Int = attributes.getInt(R.styleable.StarView_star_noOfPoints, noOfPoints)
            noOfPoints = if (points > 2) points else noOfPoints
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {

            override fun createClipPath(width: Int, height: Int): Path? {
                val vertices = noOfPoints * 2
                val alpha = (2 * Math.PI).toFloat() / vertices
                val radius = (if (height <= width) height else width) / 2
                val centerX = (width / 2).toFloat()
                val centerY = (height / 2).toFloat()

                val path = Path()
                for (i in vertices + 1 downTo 1) {
                    val r = radius * (i % 2 + 1) / 2.toFloat()
                    val omega = alpha * i.toDouble()
                    path.lineTo(
                        (r * Math.sin(omega)).toFloat() + centerX,
                        (r * Math.cos(omega)).toFloat() + centerY
                    )
                }
                path.close()
                return path
            }

        })
    }

    fun setNoOfPoints(noOfPoints: Int) {
        this.noOfPoints = noOfPoints
        postInvalidate()
    }

    fun getNoOfPoints(): Int {
        return noOfPoints
    }
}