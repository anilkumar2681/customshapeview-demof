package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R

class CircleView : CustomShapeView {

    private var borderWidthPx = 0f

    @ColorInt
    private var borderColor: Int = Color.WHITE

    private val borderPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {

        if (attrs != null) {

            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.CircleView)
            borderWidthPx = attributes.getDimensionPixelSize(
                R.styleable.CircleView_circle_borderWidth,
                borderWidthPx.toInt()
            ).toFloat()
            borderColor =
                attributes.getColor(R.styleable.CircleView_circle_borderColor, borderColor)
            attributes.recycle()
        }

        borderPaint.setAntiAlias(true)
        borderPaint.setStyle(Paint.Style.STROKE)
        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                val path = Path()

                path.addCircle(
                    width / 2f,
                    height / 2f,
                    Math.min(width / 2f, height / 2f),
                    Path.Direction.CW
                )

                return path
            }

            fun requiresBitmap(): Boolean {
                return false
            }

        })
    }


    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        if (borderWidthPx > 0) {
            borderPaint.strokeWidth = borderWidthPx
            borderPaint.color = borderColor
            canvas.drawCircle(
                width / 2f,
                height / 2f,
                Math.min(
                    (width - borderWidthPx) / 2f,
                    (height - borderWidthPx) / 2f
                ),
                borderPaint
            )
        }
    }

    fun setBorderWidthPx(borderWidthPx: Int) {
        this.borderWidthPx = borderWidthPx.toFloat()
        postInvalidate()
    }

    fun setBorderColor(@ColorInt borderColor: Int) {
        this.borderColor = borderColor
    }

    fun getBorderWidth(): Float {
        return borderWidthPx
    }

    fun getBorderColor(): Int {
        return borderColor
    }

}