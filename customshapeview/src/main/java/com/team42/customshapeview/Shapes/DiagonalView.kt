package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.util.AttributeSet
import androidx.annotation.IntDef
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R

class DiagonalView : CustomShapeView {

    companion object {
        const val POSITION_BOTTOM = 1
        const val POSITION_TOP = 2
        const val POSITION_LEFT = 3
        const val POSITION_RIGHT = 4
        const val DIRECTION_LEFT = 1
        const val DIRECTION_RIGHT = 2
    }

    @IntDef(value = [POSITION_BOTTOM, POSITION_TOP, POSITION_LEFT, POSITION_RIGHT])
    annotation class DiagonalPosition

    @IntDef(value = [DIRECTION_LEFT, DIRECTION_RIGHT])
    annotation class DiagonalDirection

    @DiagonalPosition
    private var diagonalPosition = POSITION_TOP
    private var diagonalDirection = POSITION_TOP
    private var diagonalAngle = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }


    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {

            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.DiagonalView)
            diagonalAngle =
                attributes.getInt(R.styleable.DiagonalView_diagonal_angle, diagonalAngle)
            diagonalDirection =
                attributes.getInt(R.styleable.DiagonalView_diagonal_direction, diagonalDirection)
            diagonalPosition =
                attributes.getInt(R.styleable.DiagonalView_diagonal_position, diagonalPosition)

            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                val path = Path()
                val perpendicularHeight =
                    (width * Math.tan(Math.toRadians(diagonalAngle.toDouble()))).toFloat()
                val isDirectionLeft = diagonalDirection == DIRECTION_LEFT

                when (diagonalPosition) {
                    POSITION_BOTTOM -> if (isDirectionLeft) {
                        path.moveTo(paddingLeft.toFloat(), paddingRight.toFloat())
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            (width - paddingRight).toFloat(),
                            height - perpendicularHeight - paddingBottom
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    } else {
                        path.moveTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            height - perpendicularHeight - paddingBottom
                        )
                        path.lineTo(paddingLeft.toFloat(), paddingTop.toFloat())
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.close()
                    }
                    POSITION_TOP -> if (isDirectionLeft) {
                        path.moveTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            (width - paddingRight).toFloat(),
                            paddingTop + perpendicularHeight
                        )
                        path.lineTo(paddingLeft.toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    } else {
                        path.moveTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            paddingLeft.toFloat(),
                            paddingTop + perpendicularHeight
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    }
                    POSITION_RIGHT -> if (isDirectionLeft) {
                        path.moveTo(paddingLeft.toFloat(), paddingTop.toFloat())
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            width - paddingRight - perpendicularHeight,
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    } else {
                        path.moveTo(paddingLeft.toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            width - paddingRight - perpendicularHeight,
                            paddingTop.toFloat()
                        )
                        path.lineTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    }
                    POSITION_LEFT -> if (isDirectionLeft) {
                        path.moveTo(
                            paddingLeft + perpendicularHeight,
                            paddingTop.toFloat()
                        )
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            paddingLeft.toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    } else {
                        path.moveTo(paddingLeft.toFloat(), paddingTop.toFloat())
                        path.lineTo((width - paddingRight).toFloat(), paddingTop.toFloat())
                        path.lineTo(
                            (width - paddingRight).toFloat(),
                            (height - paddingBottom).toFloat()
                        )
                        path.lineTo(
                            paddingLeft + perpendicularHeight,
                            (height - paddingBottom).toFloat()
                        )
                        path.close()
                    }
                }

                return path
            }

        })
    }

    fun setDiagonalPosition(@DiagonalPosition diagonalPosition: Int) {
        this.diagonalPosition = diagonalPosition
        postInvalidate()
    }

    fun getDiagonalPosition(): Int {
        return diagonalPosition
    }

    fun getDiagonalDirection(): Int {
        return diagonalDirection
    }

    fun setDiagonalDirection(diagonalDirection: Int) {
        this.diagonalDirection = diagonalDirection
        postInvalidate()
    }

    fun getDiagonalAngle(): Int {
        return diagonalAngle
    }

    fun setDiagonalAngle(diagonalAngle: Int) {
        this.diagonalAngle = diagonalAngle
        postInvalidate()
    }
}