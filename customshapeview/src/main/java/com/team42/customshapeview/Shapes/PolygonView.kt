package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R

class PolygonView : CustomShapeView {

    private var numberOfSides = 4

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.PolygonView)
            val sides: Int =
                attributes.getInt(R.styleable.PolygonView_polygon_noOfSides, numberOfSides)
            numberOfSides = if (sides > 3) sides else numberOfSides
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                val section = (2.0 * Math.PI / numberOfSides).toFloat()
                val polygonSize = Math.min(width, height)
                val radius = polygonSize / 2
                val centerX = width / 2
                val centerY = height / 2

                val polygonPath = Path()
                polygonPath.moveTo(
                    centerX + radius * Math.cos(0.0).toFloat(),
                    centerY + radius * Math.sin(0.0).toFloat()
                )

                for (i in 1 until numberOfSides) {
                    polygonPath.lineTo(
                        centerX + radius * Math.cos(section * i.toDouble()).toFloat(),
                        centerY + radius * Math.sin(section * i.toDouble()).toFloat()
                    )
                }

                polygonPath.close()
                return polygonPath
            }
        })
    }

    fun getNoOfSides(): Int {
        return numberOfSides
    }

    fun setNoOfSides(numberOfSides: Int) {
        this.numberOfSides = numberOfSides
        postInvalidate()
    }
}