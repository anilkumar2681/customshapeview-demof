package com.team42.customshapeview.Shapes

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.annotation.IntDef
import androidx.annotation.Nullable
import com.team42.customshapeview.CustomShapeView
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator
import com.team42.customshapeview.R


class BubbleView : CustomShapeView {

    companion object {
        const val POSITION_BOTTOM: Int = 1
        const val POSITION_TOP: Int = 2
        const val POSITION_LEFT: Int = 3
        const val POSITION_RIGHT: Int = 4
    }

    @Position
    private var position = POSITION_BOTTOM

    private var borderRadiusPx = 0
    private var arrowHeightPx = 0
    private var arrowWidthPx = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs!!) {
        init(context, attrs)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs!!, defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val attributes: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.BubbleView)
            borderRadiusPx = attributes.getDimensionPixelSize(
                R.styleable.BubbleView_bubble_borderRadius,
                dpToPx(10F)
            )
            position = attributes.getInt(R.styleable.BubbleView_bubble_arrowPosition, position)
            arrowHeightPx = attributes.getDimensionPixelSize(
                R.styleable.BubbleView_bubble_arrowHeight,
                dpToPx(10F)
            )
            arrowWidthPx = attributes.getDimensionPixelSize(
                R.styleable.BubbleView_bubble_arrowWidth,
                dpToPx(10F)
            )
            attributes.recycle()
        }

        CustomShapeView(context).setClipPathCreator(createClipPath = object : ClipPathCreator {
            override fun createClipPath(width: Int, height: Int): Path? {
                val myRect = RectF(0f, 0f, width.toFloat(), height.toFloat())
                return drawBubble(
                    myRect,
                    borderRadiusPx.toFloat(),
                    borderRadiusPx.toFloat(),
                    borderRadiusPx.toFloat(),
                    borderRadiusPx.toFloat()
                )
            }
        })
    }

    fun getPosition(): Int {
        return position
    }

    fun setPosition(@Position position: Int) {
        this.position = position
    }

    fun setBorderRadiusPx(borderRadiusPx: Int) {
        this.borderRadiusPx = borderRadiusPx
    }

    private fun drawBubble(
        myRect: RectF,
        topLeftDiameter_param: Float,
        topRightDiameter_param: Float,
        bottomRightDiameter_param: Float,
        bottomLeftDiameter_param: Float
    ): Path? {

        val path = Path()

        val topLeftDiameter = if (topLeftDiameter_param < 0) 0f else topLeftDiameter_param
        val topRightDiameter = if (topRightDiameter_param < 0) 0f else topRightDiameter_param
        val bottomLeftDiameter = if (bottomLeftDiameter_param < 0) 0f else bottomLeftDiameter_param
        val bottomRightDiameter =
            if (bottomRightDiameter_param < 0) 0f else bottomRightDiameter_param

        val spacingLeft =
            if (position == POSITION_LEFT) arrowHeightPx.toFloat() else 0.toFloat()
        val spacingTop =
            if (position == POSITION_TOP) arrowHeightPx.toFloat() else 0.toFloat()
        val spacingRight =
            if (position == POSITION_RIGHT) arrowHeightPx.toFloat() else 0.toFloat()
        val spacingBottom =
            if (position == POSITION_BOTTOM) arrowHeightPx.toFloat() else 0.toFloat()

        val left: Float = spacingLeft + myRect.left
        val top: Float = spacingTop + myRect.top
        val right: Float = myRect.right - spacingRight
        val bottom: Float = myRect.bottom - spacingBottom
        val centerX: Float = myRect.centerX()

        //Left Top
        path.moveTo(left + topLeftDiameter / 2f, top)
        if (position == POSITION_TOP) {
            path.lineTo(centerX - arrowWidthPx, top)
            path.lineTo(centerX, myRect.top)
            path.lineTo(centerX + arrowWidthPx, top)
        }

        //RIGHT, TOP
        path.lineTo(right - topRightDiameter / 2f, top)
        path.quadTo(right, top, right, top + topRightDiameter / 2)
        if (position == POSITION_RIGHT) {
            path.lineTo(right, bottom / 2f - arrowWidthPx)
            path.lineTo(myRect.right, bottom / 2f)
            path.lineTo(right, bottom / 2f + arrowWidthPx)
        }
        //RIGHT, BOTTOM
        path.lineTo(right, bottom - bottomRightDiameter / 2)
        path.quadTo(right, bottom, right - bottomRightDiameter / 2, bottom)

        if (position == POSITION_BOTTOM) {
            path.lineTo(centerX + arrowWidthPx, bottom)
            path.lineTo(centerX, myRect.bottom)
            path.lineTo(centerX - arrowWidthPx, bottom)
        }
        //LEFT, BOTTOM
        path.lineTo(left + bottomLeftDiameter / 2, bottom)
        path.quadTo(left, bottom, left, bottom - bottomLeftDiameter / 2)

        if (position == POSITION_LEFT) {
            path.lineTo(left, bottom / 2f + arrowWidthPx)
            path.lineTo(myRect.left, bottom / 2f)
            path.lineTo(left, bottom / 2f - arrowWidthPx)
        }
        path.lineTo(left, top + topLeftDiameter / 2)
        path.quadTo(left, top, left + topLeftDiameter / 2, top)
        path.close()
        return path

    }


    @IntDef(value = [POSITION_LEFT, POSITION_RIGHT, POSITION_TOP, POSITION_BOTTOM])
    annotation class Position {}
}