package com.team42.customshapeview.Managers

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.drawable.Drawable
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.team42.customshapeview.Managers.interfaces.ClipManager


class ClipDrawableManager :
    ClipManager {

    private var drawable: Drawable? = null

    fun setDrawable(drawable: Drawable?) {
        this.drawable = drawable
    }

    @NonNull
    override fun createMask(width: Int, height: Int): Bitmap {
        val mask: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas: Canvas = Canvas(mask)
        if (drawable != null) {
            drawable!!.setBounds(0, 0, width, height)
            drawable!!.draw(canvas)
        }
        return mask
    }

    @Nullable
    override fun getShadowConvexPath(): Path? {
        return null
    }

    override fun setupClipLayout(width: Int, height: Int) {

    }
}