package com.team42.customshapeview.Managers.interfaces

import android.graphics.Bitmap
import android.graphics.Path
import androidx.annotation.NonNull
import androidx.annotation.Nullable


public interface ClipManager {

    @NonNull
    fun createMask(width: Int, height: Int): Bitmap

    @Nullable
    fun getShadowConvexPath(): Path?

    fun setupClipLayout(width: Int, height: Int)
}