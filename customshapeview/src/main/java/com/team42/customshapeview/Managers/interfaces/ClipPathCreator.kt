package com.team42.customshapeview.Managers.interfaces

import android.graphics.Path

public interface ClipPathCreator {
    fun createClipPath(width: Int, height: Int): Path?
}