package com.team42.customshapeview.Managers

import android.graphics.*
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.team42.customshapeview.Managers.interfaces.ClipManager
import com.team42.customshapeview.Managers.interfaces.ClipPathCreator


class ClipPathManager :
    ClipManager {

    protected val path = Path()
    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var createClipPath: ClipPathCreator? = null

    fun ClipPathManager() {
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        paint.strokeWidth = 1f
    }

    @Nullable
    protected fun createClipPath(width: Int, height: Int): Path? {
        return if (createClipPath != null) {
            createClipPath!!.createClipPath(width, height)
        } else null
    }

    fun setClipPathCreator(createClipPath: ClipPathCreator?) {
        this.createClipPath = createClipPath
    }

    @NonNull
    override fun createMask(width: Int, height: Int): Bitmap {
        val mask: Bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas: Canvas = Canvas(mask)
        canvas.drawPath(path, paint)
        return mask
    }

    @Nullable
    override fun getShadowConvexPath(): Path? {
        return path
    }

    override fun setupClipLayout(width: Int, height: Int) {
        path.reset()
        val clipPath: Path? = this.createClipPath(width, height)
        if (clipPath != null) {
            path.set(clipPath)
        }
    }
}